from spec import m_class
from spec import c_class
from spec import i_class
from spec import minitest
from spec import microtest
from spec.gen import specgen
from spec.testing import dummytest

modules = {'m_class': m_class,
           'c_class': c_class,
           'i_class': i_class,
           'minitest': minitest,
           'microtest': microtest
           }
